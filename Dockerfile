FROM maven:3.8.6-openjdk-11 AS builder

# add pom.xml and source code
WORKDIR /src

ADD ./pom.xml pom.xml
ADD ./esaco-common esaco-common/
ADD ./esaco-app esaco-app/
ADD ./.git .git/

# package jar
RUN mvn -B -Djar.finalName=esaco-app.jar clean package

FROM openjdk:11

ENV ESACO_JAR /esaco/esaco-app.jar

RUN mkdir /esaco; mkdir -p /etc/grid-security/certificates
WORKDIR /esaco
COPY --from=builder /src/esaco-app/target/esaco-app*.jar /esaco/esaco-app.jar

CMD java $ESACO_JAVA_OPTS -jar $ESACO_JAR