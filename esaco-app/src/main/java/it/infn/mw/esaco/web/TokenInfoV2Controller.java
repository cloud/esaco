package it.infn.mw.esaco.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import it.infn.mw.esaco.model.AccessToken;
import it.infn.mw.esaco.service.TokenInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Iterator;

@RestController
public class TokenInfoV2Controller extends TokenControllerUtils {

  private static final ObjectMapper MAPPER = new ObjectMapper();

  private final TokenInfoService tokenInfoService;

  @Autowired
  public TokenInfoV2Controller(TokenInfoService tokenInfoService) {
    this.tokenInfoService = tokenInfoService;
  }

  @RequestMapping(value = "/v2/tokeninfo", method = RequestMethod.POST,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public JsonNode getUserInfo(@RequestParam(name = "token", required = false) String accessToken)
          throws JsonProcessingException
  {

    accessTokenSanityChecks(accessToken);

    AccessToken token = tokenInfoService.parseJWTAccessToken(accessToken);

    String introspection;
    String userinfo;

    if (tokenInfoService.isAccessTokenActive(token)) {
      introspection = tokenInfoService.introspectToken(accessToken);
      userinfo = tokenInfoService.getUserInfo(accessToken);
      ObjectNode introspectionObject = JsonNodeFactory.instance.objectNode();
      ObjectNode userInfoObject = JsonNodeFactory.instance.objectNode();
      if (introspection != null) {
        introspectionObject = MAPPER.readValue(introspection, ObjectNode.class);
      }
      if (userinfo != null) {
        userInfoObject = MAPPER.readValue(userinfo, ObjectNode.class);
      }

      return mergeResponses(introspectionObject, userInfoObject);
    } else {
      return MAPPER.readValue(INACTIVE_TOKEN_RESPONSE, ObjectNode.class);
    }
  }

  private JsonNode mergeResponses(JsonNode mainNode, JsonNode updateNode) {

    Iterator<String> fieldNames = updateNode.fieldNames();
    while (fieldNames.hasNext()) {

      String fieldName = fieldNames.next();
      JsonNode jsonNode = mainNode.get(fieldName);
      // if field exists and is an embedded object
      if (jsonNode != null && jsonNode.isObject()) {
        mergeResponses(jsonNode, updateNode.get(fieldName));
      }
      else {
        if (mainNode instanceof ObjectNode) {
          // Overwrite field
          JsonNode value = updateNode.get(fieldName);
          ((ObjectNode) mainNode).set(fieldName, value);
        }
      }

    }
    return mainNode;
  }

}